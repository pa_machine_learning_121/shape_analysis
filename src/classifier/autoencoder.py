
"""
https://blog.keras.io/building-autoencoders-in-keras.html
"""
import hashlib
import cv2 as cv
import tensorflow as tf
from keras.callbacks import TensorBoard
from keras.layers import Input, Dense, Conv2D, MaxPooling2D, UpSampling2D
from keras.models import Model

import paths
from common.utils import prepare_data_for_keras, cache_memory, plot_single_image


class Autoencoder:
    def __init__(self, dataset):
        (self.raw_train_data, self.raw_train_labels), (self.raw_test_data, self.raw_test_labels) = dataset
        self.model = self._train_model()

    @staticmethod
    def create_model(img_size):
        input_img = Input(shape=(img_size, img_size, 1))

        x = Conv2D(15, (3, 3), activation=tf.nn.relu, padding='same')(input_img)
        x = MaxPooling2D((2, 2), padding='same')(x)
        x = Conv2D(12, (3, 3), activation=tf.nn.relu, padding='same')(x)
        x = MaxPooling2D((2, 2), padding='same')(x)
        x = Conv2D(8, (3, 3), activation=tf.nn.relu, padding='same')(x)
        encoded = MaxPooling2D((2, 2), padding='same')(x)

        # at this point the representation is (4, 4, 8) i.e. 128-dimensional

        x = Conv2D(8, (3, 3), activation=tf.nn.relu, padding='same')(encoded)
        x = UpSampling2D((2, 2))(x)
        x = Conv2D(12, (3, 3), activation=tf.nn.relu, padding='same')(x)
        x = UpSampling2D((2, 2))(x)
        x = Conv2D(15, (3, 3), activation=tf.nn.relu, padding='same')(x)
        x = UpSampling2D((2, 2))(x)
        decoded = Conv2D(1, (3, 3), activation='sigmoid', padding='same')(x)

        autoencoder = Model(input_img, decoded)
        autoencoder.compile(optimizer='adadelta', loss='binary_crossentropy')
        autoencoder.summary()
        return autoencoder

    def _train_model(self):
        img_size = self.raw_train_data[0].shape[0]
        model = self.create_model(img_size=img_size)

        train_data = prepare_data_for_keras(data=self.raw_train_data, img_size=img_size)
        test_data = prepare_data_for_keras(data=self.raw_test_data, img_size=img_size)

        model = fit_model_cached(model=model, train_data=train_data, test_data=test_data, epochs=25)
        return model

    def plot_decoded_images(self):
        import numpy as np
        img_size = self.raw_train_data[0].shape[0]
        idx = np.random.randint(len(self.raw_test_data), size=10)

        test_data = prepare_data_for_keras(data=self.raw_test_data, img_size=img_size)[idx]
        decoded_imgs = self.model.predict(test_data)

        n = 10
        for i in range(n):
            input_img = test_data[i].reshape(img_size, img_size)
            output_img = decoded_imgs[i].reshape(img_size, img_size)

            plot_single_image(input_img, 'input')
            plot_single_image(output_img, 'output')
            cv.waitKey(0)


def fit_model_cached(model, train_data, test_data, epochs):
    model_summary = ''
    def append_to_model_summary(x):
        nonlocal model_summary
        model_summary += x

    model.summary(print_fn=lambda x: append_to_model_summary(x))
    model_hash = hashlib.md5(model_summary.encode()).hexdigest()
    print(f'model_hash={model_hash}')

    model = _fit_model_cached(model=model, train_data=train_data, test_data=test_data, epochs=epochs,
                              data_hash=len(train_data), model_hash=model_hash)
    return model


@cache_memory.cache(ignore=['model', 'train_data', 'test_data'])
def _fit_model_cached(model, train_data, test_data, epochs, data_hash, model_hash):
    model.fit(train_data, train_data,
              epochs=epochs,
              batch_size=128,
              shuffle=True,
              validation_data=(test_data, test_data),
              callbacks=[TensorBoard(log_dir='/tmp/autoencoder')])
    return model


def main():
    dataset = create_dataset(paths.DATA_DIR)
    autoencoder = Autoencoder(dataset=dataset)
    autoencoder.plot_decoded_images()



if __name__ == '__main__':
    main()
