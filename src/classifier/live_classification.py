import cv2 as cv

import paths
from classifier import simple_classifier
from common.utils import plot_single_image
from dataset_preparation.dataset_preparation import create_dataset
from mouse_drawer import single_drawer
from paths import SHAPES_DATASET_PATH


def run_live_classifier():
    dataset = create_dataset(paths.DATA_DIR)
    cf = simple_classifier.SimpleClassifier(dataset=dataset)

    while True:
        input_img_raw = single_drawer.get_drawing(title='Draw circle/square/triangle/diamond and press space')
        input_img_trimmed = single_drawer.trim_image(input_img_raw)
        best_label, probability = cf.classify_image(img=input_img_trimmed)
        plot_single_image(input_img_trimmed,
                          label=f'input_image classified as {best_label} ({round(probability*100, 1)} %)')
        key = cv.waitKey(0)
        cv.destroyAllWindows()
        if key == 'q':
            break


if __name__ == '__main__':
    run_live_classifier()
