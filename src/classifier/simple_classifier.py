import hashlib
import pprint

import numpy
import tensorflow as tf
import pickle
import numpy as np
from numpy.random import permutation
from keras.models import Sequential
from keras.layers import Dense, Conv2D, Dropout, Flatten, MaxPooling2D
import cv2 as cv

import paths
from common.utils import plot_single_image, cache_memory, prepare_data_for_keras
from dataset_preparation.dataset_preparation import create_dataset
from paths import SHAPES_DATASET_PATH


class SimpleClassifier:
    def __init__(self, dataset, verbose=False):
        self.verbose = verbose
        self.input_shape = None
        (self.raw_train_data, self.raw_train_labels), (self.raw_test_data, self.raw_test_labels) = dataset
        self.labels_mapping = {label: i for i, label in enumerate(sorted(set(self.raw_train_labels)))}
        self.inv_labels_mapping = {v: k for k, v in self.labels_mapping.items()}
        for k, v in self.inv_labels_mapping.items():
            print(f'# {k}: {v}')
        self.model = self._train_model()

    @staticmethod
    def create_model(img_size, output_size):
        # Creating a Sequential Model and adding the layers
        input_shape = (img_size, img_size, 1)
        model = Sequential()
        model.add(Conv2D(filters=int(img_size/2), kernel_size=(3, 3), input_shape=input_shape))
        model.add(MaxPooling2D(pool_size=(2, 2)))
        model.add(Conv2D(filters=int(img_size/2), kernel_size=(3, 3)))
        model.add(MaxPooling2D(pool_size=(2, 2)))
        model.add(Flatten())  # Flattening the 2D arrays for fully connected layers
        model.add(Dense(12, activation=tf.nn.sigmoid))  # needs o be sigmoid to get output probabilities (same for las layer),  but relu gives better classification accuracy
        model.add(Dropout(0.2))
        model.add(Dense(output_size, activation=tf.nn.sigmoid))  # using also sigmoid here gives other prediction percentage sum

        model.compile(optimizer='adam',
                      loss='sparse_categorical_crossentropy',
                      metrics=['accuracy'])
        model.summary()
        return model

    @staticmethod
    def labels_to_numbers(labels, mapping):
        return [mapping[l] for l in labels]

    def _train_model(self):
        if self.verbose:
            image_index = 1
            plot_single_image(img=self.raw_train_data[image_index],
                              label=f'Train image {image_index}: {self.inv_labels_mapping[self.raw_test_labels[image_index]]}')

        img_size = self.raw_train_data[0].shape[0]
        assert self.raw_train_data[0].shape[0] == self.raw_train_data[0].shape[1]

        train_data = prepare_data_for_keras(data=self.raw_train_data, img_size=img_size)
        test_data = prepare_data_for_keras(data=self.raw_test_data, img_size=img_size)
        train_labels_numbers = self.labels_to_numbers(self.raw_train_labels, self.labels_mapping)
        test_labels_numbers = self.labels_to_numbers(self.raw_test_labels, self.labels_mapping)

        print('Number of images in x_train', train_data.shape[0])
        print('Number of images in x_test', test_data.shape[0])

        self.input_shape = (img_size, img_size, 1)
        model = self.create_model(img_size=img_size, output_size=len(self.labels_mapping))
        model = fit_model_cached(model=model, train_data=train_data, train_labels_numbers=train_labels_numbers, epochs=6)
        eval_res = model.evaluate(test_data, test_labels_numbers)
        print('test loss, test acc:', eval_res)

        # predict for single image
        if self.verbose:
            image_index = 403
            best_label, _ = self.classify_image(test_data[image_index].reshape(1, img_size, img_size, 1))
            plot_single_image(img=self.raw_test_data[image_index],
                              label=f'Test image {image_index}: {self.inv_labels_mapping[self.raw_test_labels[image_index]]}. '
                                    f'Prediction: {best_label}')
        return model

    def classify_image(self, img):
        img_reshaped = img.reshape(1, *self.input_shape)
        prediction = self.model.predict(img_reshaped)[0]
        prediction_dict = {
            self.inv_labels_mapping[i]: f'{round(prob*100, 1)} %' for i, prob in enumerate(prediction)
        }
        print(f'prediction=')
        pprint.pprint(prediction_dict)

        probability = max(prediction)
        best_label = self.inv_labels_mapping[prediction.argmax()]
        return best_label, probability

    def plot_model_activation_imagas(self, layer_name):
        model = self.model

        from vis.visualization import visualize_activation
        from vis.utils import utils
        from keras import activations

        # Utility to search for layer index by name.
        # Alternatively we can specify this as -1 since it corresponds to the last layer.
        layer_idx = utils.find_layer_idx(model, 'dense_1')
        print(f'layer_idx={layer_idx}')

        # Swap softmax with linear
        model.layers[layer_idx].activation = activations.linear
        model = utils.apply_modifications(model)

        # This is the output node we want to maximize.
        for filter_idx in range(16):
            img = visualize_activation(model, layer_idx, filter_indices=filter_idx)
            plot_single_image(img, 'a')
            cv.waitKey(0)


def fit_model_cached(model, train_data, train_labels_numbers, epochs):
    model_summary = ''
    def append_to_model_summary(x):
        nonlocal model_summary
        model_summary += x

    model.summary(print_fn=lambda x: append_to_model_summary(x))
    model_hash = hashlib.md5(model_summary.encode()).hexdigest()
    print(f'model_hash={model_hash}')

    model = _fit_model_cached(model=model, train_data=train_data, train_labels_numbers=train_labels_numbers, epochs=epochs,
                              data_hash=len(train_data), model_hash=model_hash)
    return model


@cache_memory.cache(ignore=['model', 'train_data', 'train_labels_numbers'])
def _fit_model_cached(model, train_data, train_labels_numbers, epochs, data_hash, model_hash):
    model.fit(x=train_data, y=train_labels_numbers, epochs=epochs)
    return model


def main():
    dataset = create_dataset(paths.DATA_DIR)
    cf = SimpleClassifier(dataset=dataset)
    cf.plot_model_activation_imagas(layer_name='dense_1')

if __name__ == '__main__':
    main()
