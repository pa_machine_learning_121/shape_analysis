import os

import paths
from classifier.live_classification import run_live_classifier
from dataset_preparation.dataset_preparation import create_dataset


def main():
    dataset_path = create_dataset(base_dir=os.path.join(paths.DATA_DIR, 'processed'))
    run_live_classifier(dataset_path=dataset_path)


if __name__ == '__main__':
    main()
