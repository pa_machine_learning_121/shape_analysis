import os
import pickle
import random
import joblib

import cv2 as cv
import numpy as np
from numpy.random import permutation

import paths
from common.utils import cache_memory
from mouse_drawer.single_drawer import get_drawing, trim_image


def get_img_with_salt_pepper_noise(image, prob):
    # https://stackoverflow.com/questions/22937589/how-to-add-noise-gaussian-salt-and-pepper-etc-to-image-in-python-with-opencv
    '''
    Add salt and pepper noise to image
    prob: Probability of the noise
    '''
    output = np.zeros(image.shape,np.uint8)
    thres = 1 - prob
    for i in range(image.shape[0]):
        for j in range(image.shape[1]):
            rdn = random.random()
            if rdn < prob:
                output[i][j] = 0
            elif rdn > thres:
                output[i][j] = 255
            else:
                output[i][j] = image[i][j]
    return output


def get_img_with_gauss_noise(img, var):
    row, col = img.shape
    mean = 0
    sigma = var ** 0.5
    gauss = np.random.normal(mean, sigma, (row, col))
    gauss = gauss.reshape(row, col)
    noisy = img + gauss
    return noisy


def augment_raw_image(raw_image,
                      output_img_size: int = 32,
                      max_perspective_distortion: float = 0.015,
                      max_salt: float = 0.1,
                      max_gaussian: float = 0.15,
                      number_of_images_per_distortion=5
                      ):
    """Augment raw images.
    First perspective distortion is applied, then gaussian blur or salt/pepper
    Perspective distorion allows for rotation/affine transform

    :param raw_image:
    :param output_img_size:
    :param max_perspective_distortion:
    :param max_gaussian:
    :param max_salt:
    :param number_of_images_per_distortion:
    :return:
    """
    raw_size = raw_image.shape[0]
    assert raw_image.shape[0] == raw_image.shape[1]
    works_size = max(raw_image.shape) * 3
    workspace_img = np.zeros([works_size, works_size],dtype=np.uint8)
    workspace_img[raw_size:2*raw_size, raw_size:2*raw_size] = raw_image

    augmented_images = []
    augmented_images.append(trim_image(input_img=raw_image))

    for i in range(number_of_images_per_distortion):
        transformed_img = get_randomly_wrapped_perspective_image(workspace_img,
                                                                 factor=max_perspective_distortion)
        # cv.imshow('transformed_img', transformed_img)
        # cv.waitKey(0)
        if cv.boundingRect(transformed_img) == (0, 0, 0, 0):
            print('Skipping transformed image due to 0 size')
            continue

        transformed_trimmed_img = trim_image(input_img=transformed_img)
        augmented_images.append(transformed_trimmed_img)

        img_blur = cv.blur(transformed_trimmed_img, (3, 3))
        augmented_images.append(img_blur)

        for j in range(number_of_images_per_distortion):
            noise_sp_img = get_img_with_salt_pepper_noise(image=transformed_trimmed_img,
                                                          prob=random.uniform(0, max_salt))
            augmented_images.append(noise_sp_img)

            img_noise_gauss = get_img_with_gauss_noise(img=transformed_trimmed_img,
                                                       var=random.uniform(0, max_gaussian))
            augmented_images.append(img_noise_gauss)
    return augmented_images


def get_randomly_wrapped_perspective_image(img, factor):
    """

    :param img:
    :param factor: -1 to 1
    :return:
    """

    SIZE = 100
    pts1 = np.float32([[0, 0], [SIZE, 0], [0, SIZE], [SIZE, SIZE]])
    pts2 = np.copy(pts1)
    for index, x in np.ndenumerate(pts2):
        pts2[index] += factor * random.uniform(-SIZE, SIZE)

    transform_matrix = cv.getPerspectiveTransform(pts1, pts2)
    dst_img = cv.warpPerspective(img, transform_matrix, img.shape)
    return dst_img


def read_raw_images_shuffled(raw_dir_path):
    print(f'Reading raw images...')
    raw_images = []
    raw_labels = []

    for file_or_dir_name in os.listdir(raw_dir_path):
        file_or_dir_path = os.path.join(raw_dir_path, file_or_dir_name)
        if not os.path.isdir(file_or_dir_path):
            continue

        dir_path = file_or_dir_path
        dir_name = file_or_dir_name
        files_list = os.listdir(dir_path)
        for i, file_name in enumerate(files_list):
            # print(f'{i}/{len(files_list)} in {dir_name}')
            file_path = os.path.join(dir_path, file_name)
            img = cv.imread(filename=file_path, flags=cv.IMREAD_GRAYSCALE)
            raw_images.append(img)
            raw_labels.append(dir_name)

    perm = permutation(len(raw_images))
    raw_images_shuffled = []
    raw_labels_shuffled = []
    for i in perm:
        raw_images_shuffled.append(raw_images[i])
        raw_labels_shuffled.append(raw_labels[i])
    print(f'Loading raw images finished')
    return raw_images_shuffled, raw_labels_shuffled


def get_all_file_names_in_dir_recursive(raw_data_dir):
    files = []
    for r, d, f in os.walk(raw_data_dir):
        for file in f:
            files.append(os.path.join(r, file))
    return files


def create_dataset(base_dir, training_percentage=0.9, save_processed=True):
    raw_dir_path = os.path.join(base_dir, 'raw')
    files_list = get_all_file_names_in_dir_recursive(raw_dir_path)
    return _create_dataset(base_dir=base_dir,
                           training_percentage=training_percentage,
                           save_processed=save_processed,
                           files_list=files_list)


@cache_memory.cache  # remove in case of changed data
def _create_dataset(base_dir, training_percentage, save_processed, files_list):
    processed_dir_path = os.path.join(base_dir, 'processed')
    os.makedirs(processed_dir_path, exist_ok=True)

    raw_dir_path = os.path.join(base_dir, 'raw')
    print(f'Creating dataset for images in path {raw_dir_path}...')
    raw_images, raw_labels = read_raw_images_shuffled(raw_dir_path=raw_dir_path)

    all_data = []
    all_labels = []

    print(f'Augmenting images...')
    for img, label in zip(raw_images, raw_labels):
        augmented_imgaes = augment_raw_image(raw_image=img)
        all_data += augmented_imgaes
        all_labels += [label] * len(augmented_imgaes)

    split_index = int(training_percentage * len(all_labels))
    train_data = all_data[:split_index]
    test_data = all_data[split_index:]

    if save_processed:
        for i, (img, label) in enumerate(zip(all_data, all_labels)):
            file_name = f'img_{label}_{i}.png'
            file_path = os.path.join(processed_dir_path, file_name)
            cv.imwrite(file_path, img)

    train_labels = all_labels[:split_index]
    test_labels = all_labels[split_index:]

    print(f'Dataset ready!')
    return ((train_data, train_labels), (test_data, test_labels))


def main():
    # slightly rotated
    # affine transformation slightly
    # noises (salt and gaussian)
    create_dataset(paths.DATA_DIR)


def test():
    img_raw = trim_image(get_drawing())
    cv.imshow('img_raw', img_raw)
    img_affine = get_img_with_gauss_noise(img_raw, 0.2)
    cv.imshow('img_affine', img_affine)
    cv.waitKey(0)


def test_2():
    file_path = r'/home/przemek/Projects/shape_analysis/data/raw/circle/circle_0.png'
    img = cv.imread(filename=file_path, flags=cv.IMREAD_GRAYSCALE)
    SIZE = 100
    pts1 = np.float32([[0, 0], [SIZE, 0], [0, SIZE], [SIZE, SIZE]])
    pts2 = np.copy(pts1)
    factor = 0.01
    for index, x in np.ndenumerate(pts2):
        pts2[index] += factor * random.uniform(-SIZE, SIZE)
    print(pts2)

    transform_matrix = cv.getPerspectiveTransform(pts1, pts2)
    print('transform_matrix')
    dst_img = cv.warpPerspective(img, transform_matrix, img.shape)
    return dst_img


if __name__ == '__main__':
    main()
    # test()
    cv.destroyAllWindows()
