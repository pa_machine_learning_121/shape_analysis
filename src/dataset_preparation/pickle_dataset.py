import pickle


def pickle_dataset(data_with_labels, dataset_path):
    with open(dataset_path, 'wb') as handle:
        pickle.dump(data_with_labels, handle, protocol=pickle.HIGHEST_PROTOCOL)

