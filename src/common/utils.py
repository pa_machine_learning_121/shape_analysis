import cv2 as cv
import joblib
import numpy

import paths


def plot_single_image(img, label):
    label = str(label)
    cv.namedWindow(label, cv.WINDOW_NORMAL)
    cv.imshow(label, img)
    cv.resizeWindow(label, 500, 500)
    cv.waitKey(1)


def prepare_data_for_keras(data, img_size):
    # Reshaping the array to 4-dims so that it can work with the Keras API
    data = numpy.stack(data)

    data_reshaped = data.reshape(data.shape[0], img_size, img_size, 1)

    # Making sure that the values are float so that we can get decimal points after division
    data_reshaped = data_reshaped.astype('float32')

    # Normalizing the RGB codes by dividing it to the max RGB value.
    data_reshaped /= 255

    return data_reshaped


cache_memory = joblib.Memory(paths.CACHE_DIR_PATH, verbose=0)
