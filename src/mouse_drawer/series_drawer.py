import os
import re

import cv2 as cv

import paths
from mouse_drawer import single_drawer


def draw_and_save_series(base_dir: str, series_name: str = 'unnamed', count: int = 10):
    print(f'Pleas draw some {series_name}')
    processed_dir_path = os.path.join(base_dir, 'processed', series_name)
    raw_dir_path = os.path.join(base_dir, 'raw', series_name)
    os.makedirs(processed_dir_path, exist_ok=True)
    os.makedirs(raw_dir_path, exist_ok=True)
    start_number = 0

    for file_name in os.listdir(processed_dir_path):
        res = re.match(pattern=r'^.*_(\d+).png$', string=file_name)
        if res:
            last_number = int(res.groups()[0]) + 1
            start_number = max(last_number, start_number)
            print(start_number)

    for i in range(count):
        print(f'Progress: {i}/{count}')
        img_raw = single_drawer.get_drawing()
        img_trimmed = single_drawer.trim_image(img_raw)

        img_raw_path = os.path.join(raw_dir_path, f'{series_name}_{i+start_number}.png')
        img_processed_path = os.path.join(processed_dir_path, f'{series_name}_{i+start_number}.png')
        cv.imwrite(img_processed_path, img_trimmed)
        cv.imwrite(img_raw_path, img_raw)


def main():
    base_dir = paths.DATA_DIR
    draw_and_save_series(base_dir=base_dir, series_name='rectangle', count=31)


if __name__ == '__main__':
    main()
