import time

import cv2 as cv
import numpy as np

# part of code from https://github.com/opencv/opencv/blob/master/samples/python/


class Sketcher:
    def __init__(self, windowname, dests, colors_func, brush_size):
        self.prev_pt = None
        self.brush_size = brush_size
        self.windowname = windowname
        self.dests = dests
        self.colors_func = colors_func
        self.dirty = False
        self.show()
        cv.setMouseCallback(self.windowname, self.on_mouse)

    def show(self):
        cv.imshow(self.windowname, self.dests[0])

    def on_mouse(self, event, x, y, flags, param):
        pt = (x, y)
        if event == cv.EVENT_LBUTTONDOWN:
            self.prev_pt = pt
        elif event == cv.EVENT_LBUTTONUP:
            self.prev_pt = None

        if self.prev_pt and flags & cv.EVENT_FLAG_LBUTTON:
            for dst, color in zip(self.dests, self.colors_func()):
                cv.line(dst, self.prev_pt, pt, color, self.brush_size)
            self.dirty = True
            self.prev_pt = pt
            self.show()


def trim_image(input_img: np.ndarray, final_size=32):
    bounding_rect = cv.boundingRect(input_img)
    x, y, w, h = bounding_rect
    drawing_roi_img = input_img[y:y + h, x:x + w]
    max_size = max(drawing_roi_img.shape)
    square_roi_img = np.zeros([max_size, max_size],dtype=np.uint8)
    square_roi_img[:drawing_roi_img.shape[0], :drawing_roi_img.shape[1]] = drawing_roi_img
    # print(drawing_roi_img.shape)
    if drawing_roi_img.shape == (0,0):
        breakpoint()
    final_img = cv.resize(square_roi_img , (final_size, final_size), interpolation=cv.INTER_AREA)
    return final_img


def get_drawing(drawing_size=512, brush_size=12, title=None):
    print('Draw an image.\nPress "r" key to reset and space to finish')
    img = np.zeros([drawing_size, drawing_size],dtype=np.uint8)

    img_mark = img.copy()
    mark = np.zeros(img.shape[:2], np.uint8)
    if title:
        window_name = title
    else:
        window_name = f'drawing_sketch_{time.time()}'

    sketch = Sketcher(window_name, [img_mark, mark], lambda : ((255, 255, 255), 255), brush_size=brush_size)

    while True:
        ch = cv.waitKey()
        if ch == 27:
            break
        if ch == ord(' '):
            cv.destroyWindow(window_name)
            return img_mark
        if ch == ord('q'):
            raise Exception('Exit requested!')
        if ch == ord('r'):
            img_mark[:] = img
            mark[:] = 0
            sketch.show()


if __name__ == '__main__':
    drawing = get_drawing()
    cv.destroyAllWindows()
