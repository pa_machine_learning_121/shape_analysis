import os

DATA_DIR = os.path.abspath(os.path.join(os.path.dirname(__file__), '..', 'data'))

SHAPES_DATASET_PATH = os.path.join(DATA_DIR, 'processed', 'dataset_7992.pickle')

CACHE_DIR_PATH = '/tmp'
