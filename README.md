# Simple shape analyser.
 - Helps to draw training dataset
 - Trains model
 - Classifies live drawings

# Setup for Python 3
pip install -r requirements.txt
pip install git+https://github.com/raghakot/keras-vis
